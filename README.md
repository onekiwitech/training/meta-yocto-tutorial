# meta yocto tutorial

## setup
- ➤ `mkdir yocto`
- ➤ `cd yocto`
- *yocto* ➤ `git clone https://git.yoctoproject.org/poky -b dunfell`
- *yocto* ➤ `cd poky`
- *yocto/poky* ➤ `git clone https://gitlab.com/onekiwitech/training/meta-yocto-tutorial.git -b dunfell`
- *yocto/poky* ➤ `cd ../`
- *yocto* ➤ `source poky/oe-init-build-env`
- *yocto/build* ➤ `bitbake-layers add-layer ../poky/meta-yocto-tutorial`
- *yocto/build* ➤ `bitbake image-yocto-tutorial --runall=fetch`
- *yocto/build* ➤ `bitbake -k image-yocto-tutorial`
- *yocto/build* ➤ `runqemu qemux86-64`
  - ℹ️ Sau đó nhập mật khẩu của máy tính của bạn
  - ℹ️ Để đăng nhập máy ảo gõ **root**
  - ℹ️ Có thể chạy qemu trên terminal `runqemu qemux86-64 nographic`, Để thoát gõ lệnh <kbd>Ctrl + A</kbd> <kbd>X</kbd>