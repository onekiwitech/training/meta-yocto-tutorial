# Syntax and Operators

### 1. Basic Variable Setting
VARIABLE = " value"
VARIABLE = "value "

2. Setting a default value (?=)
A ?= "aval"

3. Setting a weak default value (??=)
A ??= "somevalue"
A ??= "someothervalue"

4. Immediate variable expansion (:=)
T = "123"
A := "test ${T}"
T = "456"
B := "${T} ${C}"
C = "cval"
C := "${C}append"

5. Appending (+=) and prepending (=+) With Spaces

6. Appending (.=) and Prepending (=.) Without Spaces

7. Appending and Prepending
B = "bval"
B:append = " additional data"
C = "cval"
C:prepend = "additional data "
D = "dval"
D:append = "additional data"

8. Removal
FOO = "123 456 789 123456 123 456 123 456"
FOO:remove = "123"