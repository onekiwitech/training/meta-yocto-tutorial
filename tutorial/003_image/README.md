### Creating Custom Images

- ➤ `cd yocto`
- *yocto* ➤ `mkdir poky/meta-yocto-tutorial/images`
- *yocto* ➤ `cp poky/meta/recipes-core/images/core-image-minimal-dev.bb poky/meta-yocto-tutorial/images/`
- *yocto* ➤ `mv poky/meta-yocto-tutorial/images/core-image-minimal-dev.bb poky/meta-yocto-tutorial/images/image-yocto-tutorial.bb`
- *yocto* ➤ `gedit poky/meta-yocto-tutorial/images/image-yocto-tutorial.bb`
```
require recipes-core/images/core-image-minimal.bb

DESCRIPTION = "Yocto Tutorial"
```
- `ls poky/meta*/recipes*/images/*.bb`
- *yocto* ➤ `source poky/oe-init-build-env`
- *yocto/build* ➤ `bitbake image-yocto-tutorial --runall=fetch`
- *yocto/build* ➤ `bitbake -k image-yocto-tutorial`