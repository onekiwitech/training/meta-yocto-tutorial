grep "DISTRO_CODENAME" poky/meta-poky/conf/distro/poky.conf
grep "DISTRO_VERSION" poky/meta-poky/conf/distro/poky.conf

1. Metadata is colection of:
- Configuration files (.conf)
- Recipes (.bb & .bbappend)
- Classes (.bbclass)
- Includes (.inc)

2. Poky = Bitbake + Metadata

- mkdir yocto
- cd yocto
- git clone https://git.yoctoproject.org/poky -b dunfell
- cd poky
- git checkout dunfell
- cd ../
- source poky/oe-init-build-env
- bitbake image-yocto-tutorial --runall=fetch
- bitbake -k image-yocto-tutorial
- runqemu qemux86-64 nographic

bitbake recipe -e | grep VARIABLE=
bitbake -e | grep ^BBPATH=
bitbake -e | grep ^BBLAYERS=
bitbake -e | grep BBLAYERS
bitbake core-image-minimal -e 
bitbake core-image-minimal -e | grep ^LICENCE=
bitbake -e | grep -w DISTRO_FEATURES
bitbake -e busybox | grep ^S=
bitbake -e <target> | grep ^WORKDIR=

target images
ls poky/meta*/recipes*/images/*.bb
hardware boards supported MACHINE
ls poky/meta*/conf/machine/*.conf
DISTRO
ls poky/meta*/conf/distro/*.conf

# Layer
bitbake-layers create-layer ../source/meta-mylayer
bitbake-layers add-layer ../source/meta-mylayer
bitbake-layers show-layers

Check Layer Compatibility
cd source
yocto-check-layer meta-mylayer

# Image
cd source/poky/meta
find recipes-*/ -name 'packagegroups'
bitbake -e <image_name> | grep ^IMAGE_FSTYPES=

# Recipes
bitbake -c <recipe_name> listtasks
bitbake -c cleanall -f <recipe_name>
bitbake -c fetch -f <recipe_name>
bitbake -c unpack -f <recipe_name>
bitbake -c configure -f <recipe_name>
bitbake -c compile -f <recipe_name>
bitbake -c install-f  <recipe_name>
bitbake -e <recipe_name> | grep ^WORKDIR=
bitbake -e <recipe_name> | grep ^S=
bitbake -e <recipe_name> | grep ^D=
bitbake -e <recipe_name> | grep ^PN=
bitbake -e <recipe_name> | grep ^PV=
bitbake -e <recipe_name> | grep ^PR=
bitbake -e <recipe_name> | grep ^T= log build

bitbake --show-versions
bitbake -v <target>
bitbake -g <target>
bitbake -g <target> -I glibc
bitbake -e core-image-minimal | grep ^IMAGE_ROOTFS=
bitbake -e | grep ^SANITY_TESTED_DISTROS=
bitbake -e | grep SANITY_TESTED_DISTROS=\"
Patches recipe
git shown HEAD > my-patch.patch

git add file_name
git commit -m "first patch"
git shown HEAD
git format-patch -1

01 FILES and PACKAGES
meta-yocto-tutorial