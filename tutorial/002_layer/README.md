# Layer

### Manually Creating Layer
- ➤ `cd yocto/poky`
- *yocto/poky* ➤ `mkdir -p meta-manual-layer/conf`
- *yocto/poky* ➤ `cp meta-skeleton/conf/layer.conf meta-manual-layer/conf/`
- edit **meta-manual-layer/conf/layer.conf**
```
# We have a conf and classes directory, add to BBPATH
BBPATH .= ":${LAYERDIR}"

# We have recipes-* directories, add to BBFILES
BBFILES += "${LAYERDIR}/recipes-*/*/*.bb ${LAYERDIR}/recipes-*/*/*.bbappend"

BBFILE_COLLECTIONS += "manual-layer"
BBFILE_PATTERN_manual-layer = "^${LAYERDIR}/"
BBFILE_PRIORITY_manual-layer = "5"

# This should only be incremented on significant changes that will
# cause compatibility issues with other layers
LAYERVERSION_manual-layer = "1"

LAYERDEPENDS_manual-layer = "core"

LAYERSERIES_COMPAT_manual-layer = "dunfell"
```
- - ➤ `cd yocto`
- *yocto* ➤ `source poky/oe-init-build-env`
- *yocto/build* ➤ `vim conf/bblayers.conf` add **meta-manual-layer**
```
BBLAYERS ?= " \
  /home/vanson/working/yocto/poky/meta \
  /home/vanson/working/yocto/poky/meta-poky \
  /home/vanson/working/yocto/poky/meta-yocto-bsp \
  /home/vanson/working/yocto/poky/meta-yocto-tutorial \
  /home/vanson/working/yocto/poky/meta-manual-layer \
  "
```
- *yocto/build* ➤ `bitbake-layers show-layers`

### Creating Layer using bitbake-layers command
- ➤ `cd yocto`
- *yocto* ➤ `source poky/oe-init-build-env`
- *yocto/build* ➤ `bitbake-layers create-layer ../poky/meta-yocto-tutorial`
- *yocto/build* ➤ `bitbake-layers add-layer ../poky/meta-yocto-tutorial`
- *yocto/build* ➤ `bitbake-layers show-layers`

### Script to check Layer Compatibility
- ➤ `cd yocto`
- *yocto* ➤ `source poky/oe-init-build-env`
- *yocto/build* ➤ `yocto-check-layer ../poky/meta-yocto-tutorial`
